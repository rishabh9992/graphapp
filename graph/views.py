# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404, render, redirect

from django.http import HttpResponse

from .models import Graph, Node

import util 

import csv

from django.http import Http404
# Create your views here.


def index(request):
	return HttpResponse("This is graph App")

#view to list all graphs
def list_graphs(request):
	graphs = Graph.objects.all()
	data = [{"name":"Eve","parent":''},{"name":"Todd","parent":"Eve"},{"name":"Ted","parent":"Eve"},{"name":"Tess","parent":"Todd"},{"name":"Trish","parent":"Ted"}]
	return render(request, 'graph/index.html',{"data":data, "graphs" : graphs})

#render individual graph
def show_graph(request, graph_id):
    try:
        graph = Graph.objects.get(pk=graph_id)
    except Graph.DoesNotExist:
        raise Http404("Graph does not exist")
    nodes = Node.objects.all().filter(graph=graph)

    return render(request,'graph/show_graph.html',{"nodes":nodes,"graph":graph})

# render add graph form
def show_add_graph(request):

	return render(request, 'graph/add_graph.html')

# post method for add graph
def add_graph(request):
	graph_name = request.POST.get('name',None)
	if not util.get_graph_by_name(graph_name):
		graph = Graph.objects.create(name=graph_name)
		node = Node.objects.create(name=graph_name,graph=graph,parent=None)
	else:
		return render(request, 'graph/add_graph.html',{"error_message":"Graph already exists"})

	return redirect(list_graphs)
	
# render add node form
def show_add_node(request, graph_id, parent_node_id):

	return render(request, 'graph/add_node.html',{'graph_id':graph_id,'node_id':parent_node_id} )

# post method for add node
def add_node(request, graph_id, parent_node_id):
	graph = Graph.objects.get(pk=graph_id)
	parent = Node.objects.get(pk=parent_node_id)
	node_name = request.POST.get('name')
	if not util.check_for_node(graph, parent, node_name):
		node = Node.objects.create(name=node_name, graph=graph, parent=parent, parent_name=parent.name, level=int(parent.level+1))
	else:
		return render(request, 'graph/add_node.html', {"error_message":"Node already exists"})
	return redirect(show_graph, graph_id = graph_id)



