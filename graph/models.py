# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# Graph Model
class Graph(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

# Node Model
class Node(models.Model):
	name = models.CharField(max_length=100)
	graph = models.ForeignKey(Graph)
	parent = models.ForeignKey('self',null=True)
	parent_name = models.CharField(max_length=100,null=True)
	level = models.IntegerField(default=0)

	def __str__(self):
		return self.name
