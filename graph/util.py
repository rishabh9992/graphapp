from .models import Graph, Node

def get_graph_by_name(graph_name):
	try:
		graph = Graph.objects.get(name=str(graph_name))
	except Graph.DoesNotExist:
		graph = None
	return graph

def check_for_node(graph, parent, node_name):
	try:
		node = Node.objects.get(graph=graph,parent=parent,name=node_name)
	except Node.DoesNotExist:
		node = None
	return node