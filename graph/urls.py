from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^list/$', views.list_graphs, name='list_graphs'),
    url(r'^show_graph/(?P<graph_id>[0-9]+)/$', views.show_graph, name='show_graph'),
    url(r'^show_add_graph/$', views.show_add_graph, name='show_add_graph'),
    url(r'^add_graph/$', views.add_graph, name='add_graph'),
    url(r'^(?P<graph_id>[0-9]+)/(?P<parent_node_id>[0-9]+)/show_add_node/$', views.show_add_node, name='show_add_node'),
    url(r'^(?P<graph_id>[0-9]+)/(?P<parent_node_id>[0-9]+)/add_node/$', views.add_node, name='add_node'),
]