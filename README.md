Before starting create a virtual environment using virtualenv command, after that activate that and follow the steps as follows:-

a.) Install packages using command pip install -r requirements.txt
b.) Once setup is done, start the server using the python manage.py runserver.
c.) Post that goto URL http://localhost:8000/graph/list/ in the browser.
d.) There a list of graph would be displayed indicating the graphs.
e.) Also there would be a Add graph link to add a new graph. Clicking on that would bring up a form, from which one can add new graphs.
f.) Click on any item from the list, that would bring up a new page with the graph layout.
g.) Clicking on each node would allow to add elements to that node.
